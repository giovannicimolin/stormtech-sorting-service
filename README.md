# BookSortingService - Stormtech Technical Assessment #

This is a simple webservice for ordering books according to the order specified on the parameters.

The software is written in Python provides a web interface and a API function to sort the books. It also makes use of pandas library (a high-performance, easy-to-use data structures and data analysis tools) and Flask (a web micro framework).

The application servers used in this build are ngix and uwsgi, and a supervisor is used to make sure everything runs fine.

This app runs from a Docker container based on Ubuntu 16.04 and uses productions versions of the chosen application servers.


## Prerequisites ##
Lastest stable version of Docker.

## Setup Intructions ##
First, you'll need to clone the repository on your machine.
```bash
git clone https://gitlab.com/giovannicimolin/stormtech-sorting-service.git
```
Then enter the repository folder.
```bash
cd stormtech-sorting-service
```
Then build the docker container with the command below.
```bash
docker build -t booksorter .
```

## Running the app ##
First, run the container using the command below. If you desire, you can change the desired port for the application modifying the parameter *-p 8080:80*, switching "8080" by the desired port.
```bash
docker run --name=bookssorterservice -d -p 8080:80 booksorter
```
After that, just go to your browser and type in localhost:8080 or use the port you defined on the first step, like the example below.
```
http://localhost:8080/
```
You should see this screen:
![Image of the BooksSorter Web Interface](http://i.imgur.com/VHC4JlA.png)
To use the app, just follow the directions on the web interface. 
To use the API function please refer to the source code.

## Libraries used:

 * [pandas](http://pandas.pydata.org/) for sorting books list
 * [Flask](http://flask.pocoo.org/) for the web interface
