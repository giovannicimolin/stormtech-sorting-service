/*
Script.js

This script adds functionality to the buttons used to select the filters.

*/

//Trim side spaces from strings
function trim(s){ 
  return ( s || '' ).replace( /^\s+|\s+$/g, '' ); 
}

//Clear all options from select
function removeOptions(selectbox)
{
    var i;
    for(i = selectbox.options.length - 1 ; i >= 0 ; i--)
    {
        selectbox.remove(i);
    }
}

function addAscending() {
	//Gets combobox and text input fields
    var combobox = document.getElementById("select_sorting_attrib");
    var inputField = document.getElementById("sorting");

    //Get combobox selected value
	var selectedValue = combobox.options[combobox.selectedIndex].value;

	//Verifies if input is empty
	if (trim(inputField.value)=="")
	{
		inputField.value = selectedValue + " Ascending";
	}
	else
	{
		inputField.value = inputField.value + "," + selectedValue + " Ascending";
	}

	//Removes selected option from ordering list
	combobox.removeChild(combobox[combobox.selectedIndex]);
}

function addDescending() {
	//Gets combobox and text input fields
    var combobox = document.getElementById("select_sorting_attrib");
    var inputField = document.getElementById("sorting");

    //Get combobox selected value
	var selectedValue = combobox.options[combobox.selectedIndex].value;

	//Verifies if input is empty
	if (trim(inputField.value)=="")
	{
		inputField.value = selectedValue + " Descending";
	}
	else
	{
		inputField.value = inputField.value + "," + selectedValue + " Descending";
	}

	//Removes selected option from ordering list
	combobox.removeChild(combobox[combobox.selectedIndex]);
}

function clearFields() {
	//Gets combobox and text input fields
    var combobox = document.getElementById("select_sorting_attrib");
    var inputField = document.getElementById("sorting");
    //Clears contents
	inputField.value = "";
	//Clears all options from select
	removeOptions(combobox);

	//Re-adds sorting values 
	option = document.createElement( 'option' );
    option.value = "Edition";
    option.text = "Edition Year";
    combobox.add(option);

    option = document.createElement( 'option' );
    option.value = option.text = "Author";
    combobox.add(option);

    option = document.createElement( 'option' );
    option.value = option.text = "Title";
    combobox.add(option);
}

//Clear text input values onload to avoid errors with the buttons and sets up 
//click function calls for each button
window.onload = function(){
	//Button click setup
	document.getElementById("addAscendingButton").addEventListener("click", addAscending);
	document.getElementById("addDescendingButton").addEventListener("click", addDescending);
	document.getElementById("clearFieldsButton").addEventListener("click", clearFields);
	//Clear fields
	clearFields();
}