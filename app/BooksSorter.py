
"""
BooksSorter Class:
Takes in a books list (csv table format) and sort it
according to specified parameters.
Makes use of pandas high-performance data
structures and analysis tools (BSD Licensed).

How to use:
1. Instance BooksSorter object
2. Import csv fomatted data using importData(csv file)
3. Use getSortedData to sort books according to parameters

#### importData(file)
Accepts file in the following format:
-First line describes the column names
-Must have a book id
-Columns are comma separated

id,title,author,edition
1,BookName,BookAuthor,EdidionYear
2,BookName2,BookAuthor2,EdidionYear2

#### getSortedData(sortOrder, SortAscending)
To use you must have previously imported data.

-sortOrder parameter:
Accepts array of items to be ordered and will sort
according to the array ordering.
-sortAscending parameter:
Accepts array of booleans to select if the corresponding
parameter from sortOrder will be sorted in ascending or descending order.

###
Example: sort a list of books ordering authors
alphabetically and titles in reversed alphabethical order.

import BooksSorter
bs = BooksSorter.BooksSorter()
bs.importData("/home/user/data.csv")
bs.getSortedData(['author', 'name'], [True, False])
"""

import pandas as pd

class BooksSorter:

    def __init__(self):
        self.shelf = pd.DataFrame()

    def importData(self, file):
        try:
            self.shelf = pd.read_csv(file)
            return 0
        except:
            return 1

    def getSortedData(self, sortOrder=[], sortAscending=[]):
        try:
            # Sorts books according to specified order and returns an
            # ordered list containig the book ids
            self.shelf = self.shelf.sort_values(axis=0, by=sortOrder,
                                                ascending=sortAscending)
            return self.shelf['id'].tolist()
        except:
            return 1

    def getData(self):
        # Returns unsorted data
        return self.shelf['id'].tolist()
