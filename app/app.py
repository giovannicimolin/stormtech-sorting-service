"""
Main service for the BooksSorter: Provides API endpoints and a web interface
to use BooksSorter class.

"""

from flask import Flask, request, redirect, url_for, \
    render_template, send_from_directory
import BooksSorter


# GLOBALLY USED VARIABLES
# Declare human readable error codes for the error pages
error_codes = {
    '10': "Null  ordering parameters specified.",
    '11': "Incorrect or missing parameters.",
    '12': "Wrong or corrupted input file.",
    '13': "Missing input file.",
    '14': "Parameter spelling mistake.",
    99: "Unidentified error."
}
# Tranlate ascending/descending words to BookSorter Library interface
translate_asc_string = {
    'ascending': True,
    'descending': False
}

# Declare it's a Flask app and configure necessary variables
app = Flask(__name__)
# Instance a BooksSorter object
bs = BooksSorter.BooksSorter()


# MAIN WEB INTERFACE AND API CODE
"""
Main app web interface for direct interaction with user
"""
@app.route('/')
def upload_file():
    return render_template('upload.html')


"""
Sorting webservice API call endpoint: receives the books list and the sorting
parameters from the webpage or any app using the API.

How to use:
To get a ordered books list using this endpoind you must send a POST
message containing:
###'file': type=file

A csv file using the shown structure:
id,title,author,edition
1,Name of the book,Generic author,2002
2,Name of new book,Generic author 2, 2007

*The first line names the columns to be ordered and all values on the same line
are separated using a comma
*The csv file must have an "id" column

###'sorting': type=text
Parameters to sort the books list, not case sensitive.
Structure of the data: "Parameter order, parameter order"

Parameter: can be any column name defined in the first line of the csv file.
Order: can be "ascending" or "descending"

Example:
Author ascending, title descending

Trying to acess it without correct parameters or data will lead
to an error page
"""
@app.route('/sort', methods=['GET', 'POST'])
def sortBooksAPICall():
    if request.method == 'POST':
        # Check if the post request has any file[]
        if 'file' not in request.files:
            return redirect('/error/13')
        # Stores file in the memory
        file = request.files['file']
        # Check if the file sent is valid and can be imported
        if bs.importData(file):
            return redirect('/error/12')

        # Check if the sorting parameter is Null
        if 'sorting' not in request.form:
            return redirect('/error/10')

        # If there's no sorting order return an empty set
        if not request.form['sorting']:
            return str([])

        """
        The piece of code below translates the human readable ordering
        input to the parameters taken by the bookssorter class
        Ex:
        "Author Ascending, Title Descending" =Tranlated to=> \
                sortOrder=['author','title'] and sortAscending=[True,False]

        Test and parse parameter input
        If you type in an incorrect parameter, the app
        will return an sorting service exception
        """
        try:
            # Extracts parameters for sorting books
            orderingList = request.form['sorting'].split(',')
            # Strips leading and trailing spaces from list
            orderingList = [x.strip(' ') for x in orderingList]
            # Splits list into arguments used by the BooksSorter interface
            # sortOrder = list of columns to order
            sortOrder = [item.split(' ', 1)[0].lower()
                         for item in orderingList]

            # sortAscending = list with directions to order (True=ascending)
            sortAscending = [item.split(' ', 1)[1].lower()
                             for item in orderingList]

            sortAscending = [translate_asc_string.get(item, True)
                             for item in sortAscending]
        except:
            # Throw a incorrect parameters error
            return redirect('/error/11')

        # If the sorting arrays were correctly set up, sort data using
        # BooksSorter object
        sortedData = bs.getSortedData(sortOrder, sortAscending)

        # If any column name error ocurred, redirects to an error page
        if sortedData == 1:
            return redirect('/error/14')

        # Else returns sorted list index, acording to user selection
        return str(sortedData)

    #  Returns error if acessed without required POST Parameters
    return redirect('/error/10')


"""
API Error return interface: used by another routines to display an human
readable error code to the user.
"""
@app.route('/error/<err_code>')
def APIError(err_code):
    code_text = error_codes.get(err_code, error_codes[99])
    return 'SortingServiceException: ' + code_text


# Main app definition
if __name__ == "__main__":
    app.run(host='0.0.0.0')
